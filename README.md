This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

Simple client for GraphQL

Development:

### `yarn`
### `yarn start`
Uses tsc & nodemon under the hood
Runs the app in the development mode.<br>
Open [http://localhost:5000](http://localhost:5000) to view it in the browser.

Build:
### `yarn build`