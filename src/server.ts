import * as express from 'express';
import * as cors from 'cors';
import * as graphqlHTTP from 'express-graphql';
import {default as schema} from './schema';

const app = express();

app.use(cors());
app.use('/', graphqlHTTP({schema, graphiql: true, pretty: true}));

const port = process.env.PORT || 5000;

app.listen(port, () => {
    console.log(`Connected on port ${port}`);
});
