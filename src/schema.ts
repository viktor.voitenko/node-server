const {GraphQLObjectType, GraphQLInt, GraphQLString, GraphQLBoolean, GraphQLList, GraphQLSchema} = require('graphql');
const axios = require('axios');

const PipelineType = new GraphQLObjectType({
    name: 'Pipeline',
    fields: () => ({
        id: {
            type: GraphQLInt,
        },
        ref: {
            type: GraphQLString,
        },
        sha: {
            type: GraphQLString,
        },
        status: {
            type: GraphQLString,
        }
    })
});

const CommitShortType = new GraphQLObjectType({
    name: 'CommitShort',
    fields: () => ({
        id: {
            type: GraphQLString,
        },
        title: {
            type: GraphQLString,
        },
        message: {
            type: GraphQLString,
        },
        author_name: {
            type: GraphQLString,
        }
    })
});

const CommitDetailType = new GraphQLObjectType({
    name: 'CommitDetail',
    fields: () => ({
        id: {
            type: GraphQLString,
        },
        short_id: {
            type: GraphQLString,
        },
        title: {
            type: GraphQLString,
        },
        created_at: {
            type: GraphQLString,
        },
        message: {
            type: GraphQLString,
        },
        author_name: {
            type: GraphQLString,
        },
        committer_name: {
            type: GraphQLString,
        },
        committer_email: {
            type: GraphQLString,
        },
        last_pipeline: {
            type: PipelineType
        }
    })
});

const RootQuery = new GraphQLObjectType({
    name: 'Query',
    fields: {
        commits: {
            type: new GraphQLList(CommitShortType),
            resolve: async () => {
            const res = await getData(`${process.env.GITLAB_PROJECT_URL}commits`);
            return res.data;
            }
        },
        commit: {
            type: CommitDetailType,
            args: {
                sha: {
                    type: GraphQLString
                }
            },
            resolve: async (root, args): Promise<{}> => {
                const res = await getData(`${process.env.GITLAB_PROJECT_URL}commits/${args.sha}`);
                return res.data;
            }
        }
    }
});

const getData = (url): Promise<any> => axios.get(url,{headers: { 'PRIVATE-TOKEN': process.env.GITLAB_TOKEN }});

export default new GraphQLSchema({
    query: RootQuery
});